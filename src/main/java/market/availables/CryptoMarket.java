package market.availables;

import java.util.Random;

import market.Market;
import operation.Order;


public class CryptoMarket implements Market{

	@Override
	public double getPrice(String symbol) {
		 Random r = new Random();
	     double randomValue = 10.0 + (500000.0 - 10.0) * r.nextDouble();
	     return randomValue;
	}

	@Override
	public String getName() {
		return "CRYPTOMARKET";
	}

	@Override
	public String placeOrder(Order order) {
		return "ABCD1234";
	}
 
}
